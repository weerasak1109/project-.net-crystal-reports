# Project  .Net  Crystal Reports

url สำหรับโหลดโปรแกรม
1. https://www.sap.com/cmp/td/sap-crystal-reports-visual-studio-trial.overlay-component.html

2. https://www.tektutorialshub.com/crystal-reports/crystal-reports-download-for-visual-studio/#crystal-reports-for-visual-studio-download (อันนี้มีหลายเวอร์ชั่น ให้เลือก )

ไฟล์หรือโปรแกรมที่เกี่ยวข้องที่ต้องติดตั้ง
1.SAP Crystal Reports for Visual Studio (SP27) installation package for Microsoft Visual Studio IDE

2.SAP Crystal Reports for Visual Studio (SP27) runtime engine for .NET framework MSI (32-bit) 

3.SAP Crystal Reports for Visual Studio (SP27) runtime engine for .NET framework MSI (64-bit)

4.SAP Crystal Reports 2013 v14.1.5.1501 - full Registered (โปรแกรมทำไฟล์  Reports) อยู่ที่ APP ต่างๆ
 
ปล. วิธีเลือก Reports  runtime engine ให้ดูเวอร์ชั่นของ  CrystalDecisions.CrystalReports.Engine ว่าเป็นอะไร 
ยกตัวอย่างเช่น CrystalDecisions.CrystalReports.Engine Version 13.0.2000.0 ก็จะไปตรงกับ Service Pack 20(SP20) 
ก็ให้โหลดตัวนี้มาติดตั้ง

ขั้นตอนการทำ 
1. ติดตั้ง Microsoft Visual Studio IDE
2. ลง runtime engine ทั้ง2 ตัว
3.SAP Crystal Reports 2013 v14.1.5.1501 

ติดตั้ง NuGet
* CrystalDecisions.CrystalReports.Engine
* CrystalDecisions.Shared
* CrystalDecisions.Web 