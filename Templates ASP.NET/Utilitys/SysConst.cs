﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Utilitys
{
    public enum DBType { MSSQL = 1, ORCL = 2, MONGO = 3 }

    public enum RepType { UNKNOWN = 0, C1 = 1, CRYSTAL = 2 }

    public enum LogLevel { ALL_PROCESS = 1, OPERATION = 2, ERROR = 3 }

    public class Language
    {
        public const int EN = 1;
        public const int TH = 2;
        public const int OTHER = 3;
    }

    #region ReportPrintType 
    public class ReportPrintType
    {
        public const int PRINTER = 0;
        public const int PDF = 1;
        public const int EXCEL = 2;
        public const int COLUMNS = 6;
    }
    #endregion
}