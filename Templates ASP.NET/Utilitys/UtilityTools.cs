﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectExampleCrystalReports.Utilitys
{
    public class GetException
    {
        public static Exception getException(Exception exc, string name)
        {
            try
            {
                string message = "";

                if (name == null) name = "UtilityTools - GetException";

                if (exc.InnerException == null)
                {
                    message = string.Format(@"{0} : {1},  {2}", name, exc.Message, exc.StackTrace);
                }
                else
                    message = string.Format("{0}", exc.Message);

                return new Exception(message, exc);
            }
            catch (Exception ex)
            {
                #region Catch Exception
                string mesg = "";
                if (ex.InnerException == null)
                    mesg = string.Format("UtilityTools - GetException : {0}", ex.Message);
                else
                    mesg = string.Format("{0}", ex.Message);

                return new Exception(mesg, exc);
                #endregion
            }
        }

    }

}