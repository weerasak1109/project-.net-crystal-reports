﻿using MongoDB.Libmongocrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectExampleCrystalReports.Utilitys
{
    #region LogModel
    /// <summary>
    /// Logging Data Model
    /// </summary>
    public class LogModel
    {
        public string LogPath { get; private set; }
        public bool Log { get; private set; }
        public int LogModule { get; private set; }
        public LogLevel LogLevel { get; private set; }

        /// <summary>
        /// Logging Data Model Constructure
        /// </summary>
        /// <param name="logpath">Directory for Keep Log file</param>
        /// <param name="log">ON/OFF Log Mode - True for ON, False for OFF</param>
        /// <param name="logmodule">Log Module</param>
        /// <param name="loglevel">Level for Logging - All Process, ERROR</param>
        public LogModel(string logpath, bool log, int logmodule, LogLevel loglevel)
        {
            this.LogPath = logpath;
            this.Log = log;
            this.LogModule = logmodule;
            this.LogLevel = loglevel;
        }
    }
    #endregion
}