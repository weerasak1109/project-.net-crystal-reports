﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Templates_ASP.NET.Utilitys
{
    public class CryptoUtility
    {
        private static string KEY_3DES = "wxiKIupNwmp0edyf";
        private CryptoUtility()
        {
        }

        #region TripleDes
        public static string TripleDesEncrypt(string input)
        {
            return TripleDes(input, true);
        }
        public static string TripleDesDecrypt(string input)
        {
            return TripleDes(input, false);
        }
        private static string TripleDes(string input, bool encrypt)
        {
            try
            {
                byte[] keyarr = UTF8Encoding.UTF8.GetBytes(KEY_3DES);


                if (encrypt)
                {
                    byte[] inputarr = UTF8Encoding.UTF8.GetBytes(input);
                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                    tdes.Key = keyarr;
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    ICryptoTransform transform = tdes.CreateEncryptor();
                    byte[] resultarr = transform.TransformFinalBlock(inputarr, 0, inputarr.Length);

                    tdes.Clear();

                    return Convert.ToBase64String(resultarr, 0, resultarr.Length);

                }
                else
                {
                    byte[] inputarr = Convert.FromBase64String(input);
                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                    tdes.Key = keyarr;
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    ICryptoTransform transform = tdes.CreateDecryptor();
                    byte[] resultarr = transform.TransformFinalBlock(inputarr, 0, inputarr.Length);

                    tdes.Clear();

                    return UTF8Encoding.UTF8.GetString(resultarr);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CryptoUtility: TripleDes", ex);
            }
        }
        #endregion
    }
}