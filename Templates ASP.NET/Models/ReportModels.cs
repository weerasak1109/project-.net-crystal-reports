﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectExampleCrystalReports.Models
{
    public class RepParamEntity
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }

    public class PrintEntity
    {
        public int PrintType { get; set; } // 1 - Export To PDF, 2 - Export To Excel
    }
}