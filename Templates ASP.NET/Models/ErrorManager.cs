﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Utilitys;

namespace ProjectExampleCrystalReports.Models
{
    public class ErrorManager
    {
        private DBType _dbtype;

        public const int SUCCESS = 1;
        public const int UNKNOWN = 0;

        public const int INTERNAL_ERROR = -1000;
    }
}