﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Models
{
    public class OutputBaseModel 
    {
        public int ErrorCode { get; set; }
        public int ErrorSubCode { get; set; }

        public string Title { get; set; }
        public string ErrorMesg { get; set; }
        public string ErrorDetail { get; set; }

        public int FlowControl { get; set; }

        public Object Data { get; private set; }

        /// <summary>
        /// ข้อมูลส่งออก
        /// </summary>
        public class OutputBaseModels
        {
            /// <summary>
            /// รหัสผิดพลาด
            /// </summary>
            [Required]
            public int ErrorCode { get; set; }
            /// <summary>
            /// รหัสย่อยผิดพลาด
            /// </summary>
            [Required]
            public int ErrorSubCode { get; set; }
            /// <summary>
            /// หัวผิดพลาด
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// ข้อความผิดพลาด
            /// </summary>
            public string ErrorMesg { get; set; }
            /// <summary>
            /// รายละเอียดผิดพลาด
            /// </summary>
            public string ErrorDetail { get; set; }
            /// <summary>
            /// ควบคุมการไหล
            /// </summary>
            [Required]
            public int FlowControl { get; set; }
        }

        #region Normal Constructure
        public OutputBaseModel(Object data, int errorcode, string errmesg = "")
        {
            this.ErrorCode = errorcode;
            this.ErrorMesg = errmesg;

            this.Data = data;
        }
        #endregion

        public OutputBaseModel(int errorcode, string title = "", string errormesg = "", string errordetail = "")
        {
            this.ErrorCode = errorcode;
            this.Title = title;
            this.ErrorMesg = errormesg;
            this.ErrorDetail = errordetail;
            this.Data = new Object();
        }
    }
}