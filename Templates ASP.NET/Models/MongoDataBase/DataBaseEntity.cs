﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Models.MongoDataBase
{
    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class counters
    {
        public string t_name { get; set; }
        public long seq { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class zz_error_message
    {
        public ObjectId _id { get; set; }
        public long err_id { get; set; }
        public long err_sub { get; set; }
        public string err_code { get; set; }
        public string title1 { get; set; }
        public string err_msg1 { get; set; }
        public string err_fix1 { get; set; }
        public string title2 { get; set; }
        public string err_msg2 { get; set; }
        public string err_fix2 { get; set; }
        public string title3 { get; set; }
        public string err_msg3 { get; set; }
        public string err_fix3 { get; set; }
        public string remark { get; set; }
    }
}