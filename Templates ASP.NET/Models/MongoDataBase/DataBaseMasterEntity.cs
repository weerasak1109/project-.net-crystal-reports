﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Models.MongoDataBase
{
    public class DataBaseMasterEntity
    {
        /// <summary>
        /// authen 
        /// </summary>
        public class authen_key
        {
            /// <summary>
            /// token
            /// </summary>
            public string key { get; set; }
        }

        /// <summary>
        /// img
        /// </summary>
        public class img
        {
            /// <summary>
            /// รหัสรูปออนไลน์
            /// </summary>
            public Int64 img_id { get; set; }
            /// <summary>
            /// ที่อยู่รูปออนไลน์
            /// </summary>
            public string img_path { get; set; }

        }
        /// <summary>
        /// สถานะการใช้งาน
        /// </summary>
        public class status_online
        {
            public bool is_online { get; set; }
            public DateTime online_datetime { get; set; }
            public DateTime? offline_datetime { get; set; }
        }

        /// <summary>
        /// z_user ผู้ใช้
        /// </summary>
        [BsonIgnoreExtraElements]
        public class z_user : DataMasterEntity
        {
            /// <summary>
            /// รหัสผู้ใช้
            /// </summary>
            public Int64 user_id { get; set; }
            /// <summary>
            /// ประเภทผู้ใช้
            /// </summary>
            public Int64 user_type { get; set; }
            /// <summary>
            /// ยูสเซ่อเนม
            /// </summary>
            public string username { get; set; }
            /// <summary>
            /// รหัสผ่าน
            /// </summary>
            public string pwd { get; set; }
            /// <summary>
            /// วันที่บัญชีจะหมดอายุ
            /// </summary>
            public DateTime? lc_acc { get; set; }
            /// <summary>
            /// จำนวนครั้งที่เข้าสู่ระบบผิดพลาด
            /// </summary>
            public Int64 lf_times { get; set; }
            /// <summary>
            /// จำนวนครั้งสูงสุดที่เข้าสู่ระบบผิดพลาด
            /// </summary>
            public Int64 lf_limit { get; set; }
            /// <summary>
            /// วิธีการแจ้งให้เปลี่ยนรหัสผ่าน
            /// </summary>
            public Int64 cpw_type { get; set; }
            /// <summary>
            /// ชื่อ
            /// </summary>
            public string f_name { get; set; }
            /// <summary>
            /// นามสกุล
            /// </summary>
            public string l_name { get; set; }
            /// <summary>
            /// เบอร์มือถือ
            /// </summary>
            public string mobile { get; set; }
            /// <summary>
            /// อีเมล
            /// </summary>
            public string email { get; set; }
            /// <summary>
            /// เลขประจำตัวประชาชน
            /// </summary>
            public string id_card { get; set; }
            /// <summary>
            /// วันเกิด
            /// </summary>
            public DateTime? date_of_birth { get; set; }
            /// <summary>
            /// บ้านเลขที่ หมู่ หมู่บ้าน
            /// </summary>
            public string addr { get; set; }
            /// <summary>
            /// รับข่าวสาร
            /// </summary>
            public bool recv_news { get; set; }
            /// <summary>
            /// ล๊อกการใช้งาน
            /// </summary>
            public bool locked { get; set; }
            /// <summary>
            /// ยืนยันผู้ใช้
            /// </summary>
            public bool activated { get; set; }
            /// <summary>
            /// รหัสแจ้งเตือน
            /// </summary>
            public string push_key { get; set; }
            /// <summary>
            /// authen เดิม
            /// </summary>
            public authen_key authen { get; set; }
            /// <summary>
            /// รูปผู้ใช้งาน
            /// </summary>
            public img image { get; set; }
            /// <summary>
            /// สถานะ online / offline
            /// </summary>
            public status_online online { get; set; }
        }
    }
}