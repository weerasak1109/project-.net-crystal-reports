﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ProjectExampleCrystalReports.Models;
using ProjectExampleCrystalReports.Services.Mongo;
using ProjectExampleCrystalReports.Services.MSSQL;
using ProjectExampleCrystalReports.Services.ORCL;
using ProjectExampleCrystalReports.Utilitys;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Templates_ASP.NET.DBConnector;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Utilitys;

namespace ProjectExampleCrystalReports.Management
{
    public class ReportManager
    {
        private DBType _dbtype;
        private RepType _reptype;

        private MSSQLReportLogic _msrptlogic;
        //private ORCLReportLogic _orclrptlogic;
        //private MongoReportLogic _mongologic;

        #region Singleton
        private static ReportManager _instance = null;

        public static ReportManager I()
        {
            if (_instance == null)
                _instance = new ReportManager();
            return _instance;
        }

        private ReportManager()
        {

            _reptype = AppManager.I().ReportType;
            _dbtype = AppManager.I().DatabaseType;

           // _orclrptlogic = new ORCLReportLogic();
            _msrptlogic = new MSSQLReportLogic();
           // _mongologic = new MongoReportLogic();
        }

        #endregion

        #region Get Report Mssql
        public OutputBaseModel ReportPrint(PrintEntity printentity, out Stream reportStream)
        {
            reportStream = null;
            OutputBaseModel output = null;
            int retcode = ErrorManager.UNKNOWN;
            ReportDocument report = new ReportDocument();

            try
            {
                SqlConnection mscon = MSSQLConnector.I().NewDBConn;

                #region Get Data
                var data = _msrptlogic.GetReport(mscon,"");
                #endregion

                #region Get Report
                string path = ConfigurationManager.AppSettings["ReportPath"] == null ? "" : ConfigurationManager.AppSettings["ReportPath"];

                report.Load(path+ "ReportOutlet.rpt");
                #endregion

                #region Render Report
                ExportFormatType exporttype = ExportFormatType.PortableDocFormat;
                switch (printentity.PrintType)
                {
                    case ReportPrintType.PDF:
                        exporttype = ExportFormatType.PortableDocFormat;
                        break;
                    case ReportPrintType.EXCEL:
                        exporttype = ExportFormatType.ExcelWorkbook;
                        break;
                    default:
                        exporttype = ExportFormatType.PortableDocFormat;
                        break;
                    case ReportPrintType.COLUMNS:
                        return output = new OutputBaseModel(getColumnsDataTable(data), retcode);
                }

                report.SetDataSource(data); // โยนของเข้า report
                reportStream = report.ExportToStream(exporttype);
                report.Close();
                report.Dispose();
                #endregion

                output = new OutputBaseModel(ErrorManager.SUCCESS);

                return output;
            }
            catch (Exception ex)
            {
                Exception exc = GetException.getException(ex, "ReportManager - ReportRenderMongoDB");
                string mesg = "";
                mesg = exc.Message;

                OutputBaseModel error = new OutputBaseModel(ErrorManager.INTERNAL_ERROR, "ReportRender : INTERNAL ERROR", mesg);

                return error;
            }
        }
        #endregion

        public Hashtable getColumnsDataTable(DataTable table)
        {
            Hashtable output = new Hashtable();

            foreach (DataColumn i in table.Columns)
            {
                output.Add(i.ColumnName, i.DataType.FullName);
            }
            return output;
        }
    }
}