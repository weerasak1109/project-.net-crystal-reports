﻿using ProjectExampleCrystalReports.Utilitys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Templates_ASP.NET.Utilitys;

namespace ProjectExampleCrystalReports.Management
{
    public class AppManager
    {
        private bool _isad;
        private DBType _dbtype;
        private RepType _reptype;

        private LogModel _logmodel;

        #region Singleton
        private static AppManager _instance = null;
        /// <summary>
        /// Get Instance of AppManager 
        /// </summary>
        /// <returns>AppManager</returns>
        public static AppManager I()
        {
            if (_instance == null)
                _instance = new AppManager();
            return _instance;
        }

        /// <summary>
        /// Constructor of AppManager
        /// </summary>
        private AppManager()
        {
            _isad = Convert.ToBoolean(ConfigurationManager.AppSettings["ADLogin"]);

            #region DB Type
            string app_dbtype = Convert.ToString(ConfigurationManager.AppSettings["DBType"]);
            if (app_dbtype.ToUpper() == "ORCL")
                _dbtype = DBType.ORCL;
            else if (app_dbtype.ToUpper() == "MSSQL")
                _dbtype = DBType.MSSQL;
            else if (app_dbtype.ToUpper() == "MONGO")
                _dbtype = DBType.MONGO;
            #endregion

            #region RepType
            string app_reptype = Convert.ToString(ConfigurationManager.AppSettings["RepType"]);
            if (app_reptype.ToUpper() == "C1")
                _reptype = RepType.C1;
            else if (app_reptype.ToUpper() == "CRYSTAL")
                _reptype = RepType.CRYSTAL;
            else
                _reptype = RepType.UNKNOWN;
            #endregion



        }
        #endregion

        #region Report
        /// <summary>
        /// Get Report Type - C1, CRYSTAL
        /// </summary>
        public RepType ReportType
        {
            get { return _reptype; }
        }
        #endregion

        #region DatabaseType
        /// <summary>
        /// Get Database Type - MSSQL, ORCL
        /// </summary>
        public DBType DatabaseType
        {
            get { return _dbtype; }
        }
        #endregion
    }
}