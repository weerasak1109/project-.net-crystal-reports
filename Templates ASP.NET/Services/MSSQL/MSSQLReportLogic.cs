﻿using ProjectExampleCrystalReports.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjectExampleCrystalReports.Services.MSSQL
{
    public class MSSQLReportLogic
    {
        #region GetReportData
        public DataTable GetReport(SqlConnection sqlcon, string where)
        {
            try
            {
                SqlCommand sqlcmd = sqlcon.CreateCommand();
                int rep_tye = 1;
                if (where != "")
                {
                    rep_tye = 2;
                }
                sqlcmd.CommandText = string.Format(@"SELECT * FROM M_OUTLET", where, rep_tye);

                sqlcmd.CommandType = CommandType.Text;
                int timeout = 60;
                sqlcmd.CommandTimeout = timeout;

                DataTable result = new DataTable("List");
                SqlDataAdapter adapter = new SqlDataAdapter(sqlcmd);
                adapter.Fill(result);

                return result;

            }
            catch (Exception ex)
            {
                throw new Exception("MSSQLReportLogic: GetReportData", ex);
            }
        }
        #endregion
    }
}