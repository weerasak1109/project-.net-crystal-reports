﻿using Oracle.ManagedDataAccess.Client;
using ProjectExampleCrystalReports.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ProjectExampleCrystalReports.Services.ORCL
{
    public class ORCLReportLogic
    {
        #region GetReportData
        public DataTable GetReportData(OracleConnection sqlcon, string repname, string query, List<RepParamEntity> whereentity)
        {
            string sqlstr = null;
            return GetReportData(sqlcon, repname, query, whereentity, out sqlstr);
        }

        public DataTable GetReportData(OracleConnection sqlcon, string repname, string query, List<RepParamEntity> whereentity, out string sqlstr)
        {
            try
            {
                sqlstr = query;

                if (whereentity != null)
                {
                    foreach (RepParamEntity param in whereentity)
                    {
                        string pname = string.Format(":{0}", param.Name);
                        if (sqlstr.IndexOf(pname) > 0)
                        {
                            sqlstr = sqlstr.Replace(pname, Convert.ToString(param.Value));
                            //sqlcmd.Parameters.Add(new OracleParameter(param.Name, param.Value));
                        }
                    }
                }

                OracleCommand sqlcmd = sqlcon.CreateCommand();
                sqlcmd.CommandText = sqlstr;
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandTimeout = 9999;
                // sqlcmd.BindByName = true;

                OracleDataAdapter adapter = new OracleDataAdapter(sqlcmd);
                DataTable result = new DataTable("RepName");
                adapter.Fill(result);

                return result;

            }
            catch (Exception ex)
            {
                sqlstr = null;
                throw new Exception("ORCLReportLogic: GetReportData", ex);
            }
        }
        #endregion
    }
}