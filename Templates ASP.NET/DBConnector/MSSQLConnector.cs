﻿


using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Templates_ASP.NET.Utilitys;

namespace Templates_ASP.NET.DBConnector
{
    public class MSSQLConnector
    {
        private SqlConnection _sqlcon;
        private SqlConnection _ncsqlcon;

        private string _sqlconstr = null;

        #region Singleton
        private static MSSQLConnector _instance = null;

        public static MSSQLConnector I()
        {
            if (_instance == null)
                _instance = new MSSQLConnector();
            return _instance;
        }

        private MSSQLConnector()
        {

        }
        #endregion

        #region Close Connection
        public void CloseConn(SqlConnection sqlcon)
        {
            if (sqlcon != null)
            {
                try
                {
                    if (sqlcon.State == ConnectionState.Open)
                        sqlcon.Close();

                    sqlcon.Dispose();
                }
                catch (SqlException ex)
                {
                    //-- TODO

                }
            }
        }
        #endregion

        #region GetDBConnStr
        private string GetDBConnStr()
        {
            string connstr;
            if (_sqlconstr == null)
            {
                connstr = ConfigurationManager.ConnectionStrings["MSSQL_MAINDB"].ConnectionString;
                connstr = CryptoUtility.TripleDesDecrypt(connstr);
                _sqlconstr = connstr;
            }
            else
                connstr = _sqlconstr;

            return connstr;
        }
        #endregion

        #region Properties
        #region MainDB Connection
        public SqlConnection DBConn
        {
            get
            {
                try
                {
                    string connstr = GetDBConnStr();

                    if (_sqlcon == null)
                        _sqlcon = new SqlConnection(connstr);

                    if (_sqlcon.State == ConnectionState.Closed)
                        _sqlcon.Open();

                    return _sqlcon;
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
            }
        }

        public SqlConnection NewDBConn
        {
            get
            {
                string connstr = GetDBConnStr();


                SqlConnection sqlcon = new SqlConnection(connstr);

                sqlcon.Open();
                Application.DoEvents();

                return sqlcon;
            }
        }
        #endregion

        #endregion

    }
}