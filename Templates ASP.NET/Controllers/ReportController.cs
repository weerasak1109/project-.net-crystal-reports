﻿using ProjectExampleCrystalReports.Management;
using ProjectExampleCrystalReports.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Utilitys;

namespace ProjectExampleCrystalReports.Controllers
{
    /// <summary>
    /// Report Controller
    /// </summary>
    public class ReportController : JsonApiController
    {
        #region Report Mssql
        /// <summary>
        /// Print Report
        /// </summary>
        /// <param name="printentity"></param>
        /// <param name="langid"></param>
        /// <returns></returns>
        [Route("report/print")]
        [HttpGet]
        public HttpResponseMessage ReportPrint([FromUri] PrintEntity printentity)
        {
            Stream result = null;

            OutputBaseModel output = ReportManager.I().ReportPrint(printentity,out result);
            HttpResponseMessage response = null;

            if (output.ErrorCode == ErrorManager.SUCCESS && result != null)
            {
                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(result);

                if (printentity.PrintType == ReportPrintType.PDF)
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (printentity.PrintType == ReportPrintType.EXCEL)
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                }
            }
            else
            {
                response = new HttpResponseMessage();
                response.Content = this.CreateJsonResponseContent(output);
            }

            return response;
        }

        #endregion
    }
}