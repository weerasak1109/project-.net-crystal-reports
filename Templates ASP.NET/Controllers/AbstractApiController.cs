﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProjectExampleCrystalReports.Controllers
{
    /// <summary>
    /// Abstract Class
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AbstractApiController : ApiController
    {
    }
}