﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using Templates_ASP.NET.Models;

namespace ProjectExampleCrystalReports.Controllers
{
    /// <summary>
    /// Class for Inherit
    /// </summary>
    public class JsonApiController : AbstractApiController
    {
        /// <summary>
        /// Generate Json Output Message
        /// </summary>
        /// <param name="output">Output Data from Manager Class</param>
        /// <returns></returns>
        protected StringContent CreateJsonResponseContent(OutputBaseModel output)
        {
            return new StringContent(JObject.FromObject(output).ToString(), Encoding.UTF8, "application/json");
        }
    }
}